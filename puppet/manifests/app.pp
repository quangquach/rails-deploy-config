Exec {
  path => '/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin'
}

$lib_deps = [ #'libmagickwand-dev', 'imagemagick', # for rmagick
'postgresql-server-dev-9.1', 'postgresql-client-9.1', # for pg
'libcurl3', 'libcurl4-openssl-dev', 'nodejs'
]

package {
  $lib_deps:
    ensure => present
}

$app_name = hiera('app_name')
$env = hiera('env')
$db = hiera('db')

#
# Web part
#
$apps_path = "/webapps"
$app_path = "${apps_path}/${app_name}"
$app_share = "${app_path}/shared"

$unicorn = hiera('unicorn')
$deployer = hiera('deployer')
$deployer_group = hiera('deployer_group')

# reserve web app folder for capistrano
exec {
  'create web app folder if not existed':
    command => "/bin/mkdir -p ${app_path}",
    unless  => "/usr/bin/test -d ${app_path}";
  'create share folder if not existed':
    command => "/bin/mkdir -p ${app_share}",
    unless  => "/usr/bin/test -d ${app_share}";
  'create log folder if not existed':
    command => "/bin/mkdir -p ${app_share}/log",
    unless  => "/usr/bin/test -d ${app_share}/log";
  'create data folder if not existed':
    command => "/bin/mkdir -p ${app_share}/data",
    unless  => "/usr/bin/test -d ${app_share}/data";
  'create pids folder if not existed':
    command => "/bin/mkdir -p ${app_share}/pids",
    unless  => "/usr/bin/test -d ${app_share}/pids";
  'create config folder if not existed':
    command => "/bin/mkdir -p ${app_share}/config",
    unless  => "/usr/bin/test -d ${app_share}/config";
  'create temp files folder if not existed':
    command => "/bin/mkdir -p ${app_share}/files",
    unless  => "/usr/bin/test -d ${app_share}/files";
}

exec {
  'fix permissions for deployer':
    command => "chown ${deployer}:${deployer_group} -R ${apps_path}";
}

file {
  'patch database.yml':
    path    => "${app_path}/shared/config/database.yml",
    owner   => $deployer,
    group   => $deployer_group,
    mode    => '0644',
    content => template('database.yml.erb');
  'patch unicorn.rb':
    path    => "${app_path}/shared/config/unicorn.rb",
    owner   => $deployer,
    group   => $deployer_group,
    mode    => '0644',
    content => template('unicorn.rb.erb');
  'patch SSH Key':
    path    => "/home/${deployer}/.ssh/id_rsa",
    owner   => $deployer,
    group   => $deployer_group,
    mode    => '0644',
    content => template('vagrant');
  'patch SSH PublicKey':
    path    => "/home/${deployer}/.ssh/id_rsa.pub",
    owner   => $deployer,
    group   => $deployer_group,
    mode    => '0644',
    content => template('vagrant.pub');
  'patch SSH Config':
    path    => "/home/${deployer}/.ssh/config",
    owner   => $deployer,
    group   => $deployer_group,
    mode    => '0644',
    content => template('config.erb');
}


class { 'nginx':
  flavor => 'full',
}

nginx::vhost { $app_name:
  ensure       => present,
  listen       => '80',
#  ssl          => true,
#  ssl_redirect => true,
#  ssl_cert     => '/var/www/app/certs/example.com.crt',
#  ssl_key      => '/var/www/app/certs/example.com.key',
#  aliases      => [ 'sub1.example.com', 'sub2.example.com' ],
  doc_dir      => "${app_path}/current/public",
  log_dir      => "${app_path}/shared/log",
#  proxy_to     => '8080',
#  redirect_to  => '/app',
#  deny         => [ 'bin', 'conf' ],
  upstream     => 'unicorn'
}

nginx::upstream { 'unicorn':
  ensure => present,
  server => "unix:${app_path}/shared/sockets/unicorn.sock fail_timeout=0",
}

#
# DB part
#
class { 'postgresql::server':
  config_hash => {
    'ip_mask_deny_postgres_user' => '0.0.0.0/32',
    'ip_mask_allow_all_users' => '0.0.0.0/0',
    'listen_addresses' => '*',
    'postgres_password' => 'postgres',
  }
}

$db_name = $db['name']
postgresql::db { $db_name:
  user => $db['username'],
  password => $db['password'],
  grant => 'all'
}
