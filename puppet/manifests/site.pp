$ruby_version = 'ruby-2.0.0-p195'

$deps = ["build-essential", "openssl", "libreadline6", "libreadline6-dev", "curl", "git-core", "zlib1g", "zlib1g-dev", "libssl-dev", "libyaml-dev", "libsqlite3-dev", "sqlite3", "libxml2-dev", "autoconf", "libc6-dev", "libgdbm-dev", "ncurses-dev", "automake", "libtool", "bison", "subversion", "pkg-config", "libffi-dev", "libncurses5-dev", "imagemagick", "libmagickwand-dev"]

package {
  $deps:
    ensure => present;
}

include rvm

rvm_system_ruby {
  $ruby_version:
    ensure => present,
    default_use => true,
    require => Package[$deps];
}

rvm_gem {
  "${ruby_version}@${gemset}/puppet":
    ensure => '3.4.3',
    require => Rvm_system_ruby["${ruby_version}"];

  "${ruby_version}@${gemset}/hiera":
    ensure => '1.3.1',
    require => Rvm_system_ruby["${ruby_version}"];

  "${ruby_version}@${gemset}/hiera-puppet":
    ensure => '1.0.0',
    require => Rvm_system_ruby["${ruby_version}"];
}

rvm::system_user { vagrant: ; ubuntu: ; jenkins: ;}
